# frozen_string_literal: true
require 'logger'
require 'csv'
require 'rest-client'
require 'nokogiri'

# <title> and <h1> parser
class SergioParser
  def initialize
    @logger = Logger.new(STDOUT)
  end

  def count_to_csv(file)
    CSV.open('result.csv', 'wb') do |csv|
      CSV.foreach(file) do |row|
        @logger.info(row[0].inspect)
        error, html = parse_html(row)
        title = html_title(html, error)
        h1 = html_h1(html, error)
        csv << [row[0], title, h1]
      end
    end
  end

  private

  def parse_html(row)
    error = nil
    html = Nokogiri::HTML::Document.new
    begin
      resp = RestClient.get row[0]
      @logger.info(resp.code)
      html = Nokogiri::HTML(resp.to_str)
    rescue => e
      error = e.to_s
    end
    [error, html]
  end

  def html_title(html, error)
    res = error.nil? ? html.css('title').text : error
    @logger.info("TITLE: #{res}")
    res
  end

  def html_h1(html, error)
    res = error.nil? ? html.css('h1').map(&:text) : ''
    @logger.info("H1: #{res}")
    res
  end
end

p = SergioParser.new
p.count_to_csv ARGV[0]
